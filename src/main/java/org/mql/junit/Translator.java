package org.mql.junit;

import java.util.Map;

public class Translator {
	private String defaultLang;
	private Map<String, Map<String, String>> dict;
	
	public Translator(String defaultLang) {
		super();
		this.defaultLang = defaultLang;
	}

	public String translate(String word, String lang) {
		return dict.get(lang).get(word);
	}
	
	public String getDefaultLang() {
		return defaultLang;
	}

	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}

	public Map<String, Map<String, String>> getDict() {
		return dict;
	}

	public void setDict(Map<String, Map<String, String>> dict) {
		this.dict = dict;
	}
}
