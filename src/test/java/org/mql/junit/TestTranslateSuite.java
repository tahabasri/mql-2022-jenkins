package org.mql.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestTranslatorFr.class,
	TestTranslatorEs.class
})
public class TestTranslateSuite {

}
