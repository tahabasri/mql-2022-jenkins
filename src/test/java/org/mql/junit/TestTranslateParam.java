package org.mql.junit;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestTranslateParam {
	private Translator tr;
	private Map<String, Map<String, String>> dict;
	
	private String lang;
	private String helloTr;
	private String workTr;
	
	@Parameters
	public static Collection<Object[]> params() {
		return Arrays.asList(new Object[][] {
			// es new TestTranslateParam("", "", "") 
			{"es", "hola", "trabajar"},
			// fr new TestTranslateParam("", "", "")
			{"fr", "salut", "travailler"}
		});
	}
	
	public TestTranslateParam(String lang, String helloTr, String workTr) {
		this.lang = lang;
		this.helloTr = helloTr;
		this.workTr = workTr;
	}

	@Before
	public void before() {
		this.tr = new Translator(lang);
		dict = new HashMap<String, Map<String,String>>();
		Map<String, String> esMap = new HashMap<String, String>();
			esMap.put("work", "trabajar");
			esMap.put("hello", "hola");
		dict.put("es", esMap);
		Map<String, String> frMap = new HashMap<String, String>();
			frMap.put("work", "travailler");
			frMap.put("hello", "salut");
		dict.put("fr", frMap);
		tr.setDict(dict);
	}
	
	@Test
	public void translateHello() {
		assertEquals(helloTr, tr.translate("hello", lang));
	}
	
	@Test
	@Ignore("ignore for education purpose")
	public void translateWork() {
		assertEquals(workTr, tr.translate("work", lang));
	}
}
