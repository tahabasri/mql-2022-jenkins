package org.mql.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TestTranslatorFr {
	private Translator tr;
	private Map<String, Map<String, String>> dict;

	@Before
	public void before() {
		this.tr = new Translator("fr");
		dict = new HashMap<String, Map<String,String>>();
		Map<String, String> frMap = new HashMap<String, String>();
			frMap.put("work", "travailler");
			frMap.put("hello", "salut");
		dict.put("fr", frMap);
		tr.setDict(dict);
	}
	
	@Test
	@Ignore("ignore for education purpose")
	public void translateHello() {
		assertEquals("salut", tr.translate("hello", "fr"));
	}
	
	@Test
	@Ignore("ignore for education purpose")
	public void translateWork() {
		assertEquals("travailler", tr.translate("work", "fr"));
	}
	
	@Test
	@Ignore("ignore for education purpose")
	public void translateMatcher() {
		assertThat(tr.translate("work", "fr"), containsString("trava"));
		assertEquals("travailler", tr.translate("work", "fr"));
	}
	
	@Test(expected = NullPointerException.class)
	@Ignore("ignore for education purpose")
	public void translateUnfoundLang() {
		assertEquals("travailler", tr.translate("work", "ar"));
	}
	
	// @Test(timeout = 1000)
	// @Ignore("ignore for education purpose")
	// public void translateTimeout() {
	// 	for (;;) {
			
	// 	}
	// 	//assertEquals("travailler", tr.translate("work", "ar"));
	// }
	
	@Test
	@Ignore("ignore for education purpose")
	public void translateUnfoundLangClassic() {
		try {
			assertEquals("travailler", tr.translate("work", "ar"));	
		} catch (NullPointerException e) {
			assertNotNull(e);
		}
	}
	
}
