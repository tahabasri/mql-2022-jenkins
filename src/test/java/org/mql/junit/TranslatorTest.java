package org.mql.junit;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Result;

public class TranslatorTest {
	private Translator tr;
	private Map<String, Map<String, String>> dict;

	@Before
	public void before() {
		System.out.println(">>> setUp");
		this.tr = new Translator("es");
		dict = new HashMap<String, Map<String,String>>();
		
		Map<String, String> esMap = new HashMap<String, String>();
			esMap.put("work", "trabajar");
			esMap.put("hello", "hola");
		dict.put("es", esMap);
		
		Map<String, String> frMap = new HashMap<String, String>();
			frMap.put("work", "travailler");
			frMap.put("hello", "salut");
		dict.put("fr", frMap);
		tr.setDict(dict);
	}
	
	@Test
	public void translate() {
		assertEquals("salut", tr.translate("hello", "fr"));
	}
	
	public static void main(String[] args) {
		//org.junit.runner.JUnitCore.runClasses(TranslatorTest.class);
		Result result = org.junit.runner.JUnitCore.runClasses(TranslatorTest.class);
		System.out.println(String.format(
				"Run %d tests and found %d failures"
				, result.getRunCount(), result.getFailureCount()));
	}
	
}
