package org.mql.junit;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TestTranslatorEs {
	private Translator tr;
	private Map<String, Map<String, String>> dict;

	@Before
	public void before() {
		this.tr = new Translator("es");
		dict = new HashMap<String, Map<String,String>>();
		Map<String, String> frMap = new HashMap<String, String>();
			frMap.put("work", "trabajar");
			frMap.put("hello", "hola");
		dict.put("es", frMap);
		tr.setDict(dict);
	}
	
	@Test
	public void translateHello() {
		assertEquals("hola", tr.translate("hello", "es"));
	}
	
	@Test
	public void translateWork() {
		assertEquals("trabajar", tr.translate("work", "es"));
	}
	
}
